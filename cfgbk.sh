#!/bin/bash

## Backup profile config files.

declare -A files

files=( [config-alacritty]="$HOME/.config/alacritty" [config-nvim]="$HOME/.config/nvim" [local-nvim]="$HOME/.local/share/nvim" )

for key in ${!files[@]}
do
    mkdir -p ~/Dropbox/popos-dotfiles/$key
    rsync -av ${files[${key}]} ~/Dropbox/pop-dotfiles/$key
done
